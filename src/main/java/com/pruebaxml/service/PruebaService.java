package com.pruebaxml.service;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.stereotype.Service;

import com.pruebaxml.model.Prueba;

@Service
public class PruebaService {
	
	public StringWriter generateXml(Prueba prueba) throws JAXBException {
		try {
		    // create an instance of `JAXBContext`
		    JAXBContext context = JAXBContext.newInstance(Prueba.class);

		    // create an instance of `Marshaller`
		    Marshaller marshaller = context.createMarshaller();

		    // enable pretty-print XML output
		    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		    // write XML to `StringWriter`
		    StringWriter sw = new StringWriter();

		    // convert book object to XML
		    marshaller.marshal(prueba, sw);

		    // print the XML
		    System.out.println(sw.toString());
		    
		    return sw;

		} catch (JAXBException ex) {
		    ex.printStackTrace();
		    throw ex;
		}
	}

}

package com.pruebaxml.controller;

import java.io.StringWriter;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pruebaxml.model.Oferta;
import com.pruebaxml.model.Prueba;
import com.pruebaxml.service.PruebaService;

@RestController
public class PruebaController {
	
	@Autowired
	private PruebaService pruebaService = new PruebaService();
	
	@GetMapping("/prueba")
	public StringWriter getOption() throws JAXBException {
		
		Prueba prueba = new Prueba();
		prueba.setXmlns("http://omel.schemas.org");
		prueba.setIdMensaje("mesajeofertasmd_20102020");
		prueba.setVerMensaje("1");
		//prueba.setIdRemitente("EGLRE");
		Oferta oferta = new Oferta();
		oferta.setV("Oferta1");
		prueba.setOferta(oferta);
		return pruebaService.generateXml(prueba);
		
	}

}

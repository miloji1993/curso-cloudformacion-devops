package com.pruebaxml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaXmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaXmlApplication.class, args);
	}

}

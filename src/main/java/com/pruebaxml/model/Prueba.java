package com.pruebaxml.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MensajeOfertas")
public class Prueba {
	
	@XmlAttribute
	private String xmlns;
	
	@XmlElement(name = "IdMensaje")
	private String idMensaje;
	
	@XmlElement(name = "VerMensaje")
	private String verMensaje;
	
	@XmlElement(name = "IdRemitente")
	private String idRemitente;
	
	@XmlElement
	private Oferta oferta;
	
	

}
